import sbt._

object Dependencies {
  lazy val akka = "com.typesafe.akka" %% "akka-actor" % "2.5.19"
  lazy val akkaTyped = "com.typesafe.akka" %% "akka-actor-typed" % "2.5.19"
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % "2.5.19"
  lazy val akkaHttp = "com.typesafe.akka" %% "akka-http" % "10.1.5"
  lazy val akkaPersistence = "com.typesafe.akka" %% "akka-persistence-typed" % "2.5.19"
  lazy val levelDbJni = "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"
  //lazy val vkapi = "com.vk.api" % "sdk" % "0.5.12"
  lazy val circleCore = "io.circe" %% "circe-core" % "0.10.0"
  lazy val circleGeneric = "io.circe" %% "circe-generic" % "0.10.0"
  lazy val circleParser = "io.circe" %% "circe-parser" % "0.10.0"
  lazy val circeAkkaHttp = "de.heikoseeberger" %% "akka-http-circe" % "1.22.0"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
}
