package eu.xeppaka.bot

import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.{Behaviors, StashBuffer}
import akka.actor.typed.{ActorRef, Behavior, DispatcherSelector, SupervisorStrategy}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.util.{ByteString, Timeout}
import eu.xeppaka.bot.TelegramEntities._
import eu.xeppaka.bot.TelegramEntitiesDerivations._
import io.circe.Printer

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object CheckDeliveryDialog {
  sealed trait Command
  sealed trait CommandResult
  sealed trait DialogCommand extends Command

  case class ProcessMessage(msg: Message, replyTo: ActorRef[CommandResult]) extends Command
  case object ProcessMessageSuccess extends CommandResult
  case class ProcessMessageFailure(exception: Throwable) extends CommandResult

  case object AddParcel extends DialogCommand
  case object RemoveParcel extends DialogCommand
  case object ListParcels extends DialogCommand
  case object Help extends DialogCommand

  object DialogCommand {
    def parse(text: String): DialogCommand = text match {
      case "/add" => AddParcel
      case "/remove" => RemoveParcel
      case "/list" => ListParcels
      case "/help" => Help
      case "/start" => Help
      case _ => Help
    }
  }

  // json printer
  private val printer = Printer.noSpaces.copy(dropNullValues = true)
  // internal messages
  private case class DeliveryStateChanged(state: String) extends Command
  private val helpMessage =
    """
      |Supported commands:
      |/add - add parcel to a list of watched parcels
      |/list - list watched parcels
      |/remove - remove parcel from a watching list
    """.stripMargin
  private val commandsKeyboard = Some(ReplyKeyboardMarkup(
    Seq(Seq(KeyboardButton("/add"), KeyboardButton("/list"), KeyboardButton("/remove"))),
    resize_keyboard = Some(true),
    one_time_keyboard = Some(true)
  ))

  def behavior(chatId: Long, botUri: BotUri): Behavior[Command] = Behaviors.setup[Command] { ctx =>
    implicit val materializer: ActorMaterializer = ActorMaterializer()(ctx.system.toUntyped)
    implicit val executionContext: ExecutionContext = ctx.system.dispatchers.lookup(DispatcherSelector.default())
    val http = Http()(ctx.system.toUntyped)
    val stashBuffer = StashBuffer[Command](100)
    val deliveryStateAdapter: ActorRef[CzechPostDeliveryCheck.DeliveryStateChanged] = ctx.messageAdapter(stateChanged => DeliveryStateChanged(stateChanged.state))
    val czechPostDeliveryCheck = ctx.spawnAnonymous(Behaviors.supervise(CzechPostDeliveryCheck.behavior(chatId.toString, deliveryStateAdapter)).onFailure(SupervisorStrategy.restart))

    def initial: Behavior[Command] = sendMessage(SendMessage(chatId, "Waiting for a command...", reply_markup = commandsKeyboard), waitCommand, initial)

    def waitCommand: Behavior[Command] = Behaviors.receiveMessage {
      case ProcessMessage(msg, replyTo) =>
        val command = msg.text.map(text => DialogCommand.parse(text))
        replyTo ! ProcessMessageSuccess

        if (command.isDefined) {
          ctx.self ! command.get
          Behaviors.same
        } else {
          val message = SendMessage(chatId, "This command is unsupported.")
          sendMessage(message, initial, initial)
        }
      case AddParcel =>
        val message = SendMessage(chatId, "Please enter a parcel ID.")
        sendMessage(message, waitParcelId(parcelId => addParcel(parcelId)), initial)
      case RemoveParcel =>
        removeParcel(initial, initial)
      case ListParcels =>
        listParcels
      case Help =>
        val message = SendMessage(chatId, helpMessage)
        sendMessage(message, initial, initial)
      case DeliveryStateChanged(state) =>
        val message = SendMessage(chatId, state, Some("Markdown"))
        sendMessage(message, initial, initial)
      case _ =>
        Behaviors.unhandled
    }

    def addParcel(parcelId: String): Behavior[Command] = Behaviors.setup { ctx =>
      case object AddParcelSuccess extends Command
      case class AddParcelFailure(exception: Throwable) extends Command
      implicit val timeout: Timeout = 5.seconds

      ctx.ask[CzechPostDeliveryCheck.Command, CzechPostDeliveryCheck.CommandResult](czechPostDeliveryCheck)(ref => CzechPostDeliveryCheck.AddParcel(parcelId, ref)) {
        case Success(CzechPostDeliveryCheck.CommandResultSuccess) => AddParcelSuccess
        case Success(CzechPostDeliveryCheck.CommandResultFailure(exception)) => AddParcelFailure(exception)
        case Failure(exception) => AddParcelFailure(exception)
      }

      Behaviors.receiveMessage {
        case AddParcelSuccess =>
          val message = SendMessage(chatId, s"Parcel $parcelId was added to the watch list.")
          sendMessage(message, initial, initial)
        case AddParcelFailure(exception) =>
          exception match {
            case CzechPostDeliveryCheck.DuplicateParcelId(_) =>
              val message = SendMessage(chatId, s"Parcel $parcelId is in the watch list already.")
              sendMessage(message, initial, initial)
            case _ =>
              ctx.log.error(exception, "action=add_parcel result=failure")
              val message = SendMessage(chatId, s"Adding parcel failed. Please try again.")
              sendMessage(message, initial, initial)
          }
        case otherMessage =>
          stashBuffer.stash(otherMessage)
          Behaviors.same
      }
    }

    def listParcels: Behavior[Command] = Behaviors.setup { ctx =>
      case class ListParcelsSuccess(parcelsList: Set[String]) extends Command
      case class ListParcelsFailure(exception: Throwable) extends Command
      implicit val timeout: Timeout = 5.seconds

      ctx.ask[CzechPostDeliveryCheck.Command, CzechPostDeliveryCheck.ListParcelsResult](czechPostDeliveryCheck)(ref => CzechPostDeliveryCheck.ListParcels(ref)) {
        case Success(CzechPostDeliveryCheck.ListParcelsResult(parcelsList)) => ListParcelsSuccess(parcelsList)
        case Failure(exception) => ListParcelsFailure(exception)
      }

      Behaviors.receiveMessage {
        case ListParcelsSuccess(parcelsList) =>
          val messageText = "*List of your watched parcels:*\n" + (if (parcelsList.nonEmpty) parcelsList.toSeq.sorted.mkString("\n") else "(empty)")
          val message = SendMessage(chatId, messageText, Some("Markdown"))
          sendMessage(message, initial, initial)
        case ListParcelsFailure(exception) =>
          ctx.log.error(exception, "action=list_parcels result=failure chat_id={}", chatId)
          val message = SendMessage(chatId, "Failed to get a list of your watched parcels. Please try again later.")
          sendMessage(message, initial, initial)
        case otherMessage =>
          stashBuffer.stash(otherMessage)
          Behaviors.same
      }
    }

    def removeParcel(onSuccess: => Behavior[Command], onFailure: => Behavior[Command]): Behavior[Command] =
      Behaviors.setup { ctx =>
        case class ListParcelsSuccess(parcelsList: Set[String]) extends Command
        case class ListParcelsFailure(exception: Throwable) extends Command
        implicit val timeout: Timeout = 5.seconds

        ctx.ask[CzechPostDeliveryCheck.Command, CzechPostDeliveryCheck.ListParcelsResult](czechPostDeliveryCheck)(ref => CzechPostDeliveryCheck.ListParcels(ref)) {
          case Success(CzechPostDeliveryCheck.ListParcelsResult(parcelsList)) => ListParcelsSuccess(parcelsList)
          case Failure(exception) => ListParcelsFailure(exception)
        }

        Behaviors.receiveMessage {
          case ListParcelsSuccess(parcelsList) =>
            if (parcelsList.nonEmpty) {
              val keyboardButtons = parcelsList.toSeq.sorted.grouped(3).map(_.map(id => KeyboardButton(id))).toSeq
              val markup = ReplyKeyboardMarkup(keyboard = keyboardButtons, resize_keyboard = Some(true), one_time_keyboard = Some(true))
              val message = SendMessage(chatId, "Please enter a parcel id to remove.", reply_markup = Some(markup))
              sendMessage(message, waitParcelId(parcelId => removeParcelId(parcelId)), onFailure)
            } else {
              val message = SendMessage(chatId, "You don't have watched parcels. There is nothing to remove.")
              sendMessage(message, onSuccess, onFailure)
            }
          case ListParcelsFailure(exception) =>
            ctx.log.error(exception, "action=list_parcels result=failure chat_id={}", chatId)
            val message = SendMessage(chatId, "Failed to get a list of your watched parcels. Please try again later.")
            sendMessage(message, initial, initial)
          case otherMessage =>
            stashBuffer.stash(otherMessage)
            Behaviors.same
        }
      }

    def removeParcelId(parcelId: String): Behavior[Command] = Behaviors.setup { ctx =>
      case object RemoveParcelSuccess extends Command
      case class RemoveParcelFailure(exception: Throwable) extends Command
      implicit val timeout: Timeout = 5.seconds

      ctx.ask[CzechPostDeliveryCheck.Command, CzechPostDeliveryCheck.CommandResult](czechPostDeliveryCheck)(ref => CzechPostDeliveryCheck.RemoveParcel(parcelId, ref)) {
        case Success(CzechPostDeliveryCheck.CommandResultSuccess) => RemoveParcelSuccess
        case Success(CzechPostDeliveryCheck.CommandResultFailure(exception)) => RemoveParcelFailure(exception)
        case Failure(exception) => RemoveParcelFailure(exception)
      }

      Behaviors.receiveMessage {
        case RemoveParcelSuccess =>
          val message = SendMessage(chatId, s"Parcel $parcelId was removed from the watch list.")
          sendMessage(message, initial, initial)
        case RemoveParcelFailure(exception) =>
          exception match {
            case CzechPostDeliveryCheck.ParcelIdNotFound(_) =>
              val message = SendMessage(chatId, s"Parcel $parcelId is not found in the list of the watched parcels.")
              sendMessage(message, initial, initial)
            case _ =>
              ctx.log.error(exception, "action=add_parcel result=failure")
              val message = SendMessage(chatId, s"Remove of the parcel failed. Please try again.")
              sendMessage(message, initial, initial)
          }
        case otherMessage =>
          stashBuffer.stash(otherMessage)
          Behaviors.same
      }
    }

    //    def selectPostType(onFinish: PostType => Behavior[Command]): Behavior[Command] = Behaviors.receiveMessage {
    //
    //      case ProcessMessage(msg, replyTo) =>
    //        val button1 = KeyboardButton("button1")
    //        val button2 = KeyboardButton("button2")
    //        val keyboard = ReplyKeyboardMarkup(Seq(Seq(button1, button2)))
    //        val message = SendMessage(chatId, "Please enter parcel ID.", reply_markup = Some(keyboard))
    //        sendMessage(message, waitParcelId(parcelId => addParcel(parcelId)), initial)
    //    }

    def waitParcelId(onFinish: String => Behavior[Command]): Behavior[Command] = Behaviors.receiveMessage {
      case ProcessMessage(msg, replyTo) =>
        if (msg.text.isDefined) {
          val parcelId = msg.text.get
          replyTo ! ProcessMessageSuccess
          onFinish(parcelId)
        } else {
          replyTo ! ProcessMessageSuccess
          waitParcelId(onFinish)
        }
      case otherMsg =>
        stashBuffer.stash(otherMsg)
        Behaviors.same
    }

    def sendMessage(message: SendMessage, onSuccess: => Behavior[Command], onFailure: => Behavior[Command], attempt: Int = 0): Behavior[Command] = Behaviors.setup[Command] { ctx =>
      import io.circe.generic.auto._
      import io.circe.syntax._

      case object SendMessageSuccess extends Command
      case class SendMessageFailure(exception: Throwable) extends Command

      val json = printer.pretty(message.asJson)
      val request = HttpRequest(HttpMethods.POST, uri = botUri.sendMessage, entity = HttpEntity.Strict(ContentTypes.`application/json`, ByteString(json)))

      ctx.log.debug("action=send_message status=started chat_id={} message={}", chatId, json)

      Source
        .single(request)
        .initialDelay(2.seconds * attempt)
        .mapAsync(1) { request =>
          http
            .singleRequest(request)
            .transform {
              case Success(response) =>
                if (response.status.isSuccess()) {
                  Success(SendMessageSuccess)
                } else {
                  Success(SendMessageFailure(new RuntimeException(s"Error while sending message. HTTP status: ${response.status}.")))
                }
              case Failure(exception) =>
                ctx.log.error(exception, "action=send_message status=finished result=failure chat_id={}", chatId)
                Success(SendMessageFailure(exception))
            }
        }
        .to(Sink.foreach(ctx.self ! _))
        .run()

      Behaviors.receiveMessage {
        case SendMessageSuccess =>
          ctx.log.debug("action=send_message status=finished result=success chat_id={}", chatId)
          stashBuffer.unstashAll(ctx, onSuccess)
        case SendMessageFailure(exception) =>
          ctx.log.error(exception, "action=send_message status=finished result=failure chat_id={} attempt={}", chatId, attempt)

          if (attempt >= 5) {
            ctx.log.error(exception, "action=send_message result=failure message=attempts threshold exceeded")
            stashBuffer.unstashAll(ctx, onFailure)
          } else {
            sendMessage(message, onSuccess, onFailure, attempt + 1)
          }
        case otherMsg =>
          stashBuffer.stash(otherMsg)
          Behaviors.same
      }
    }

    initial
  }
}
