package eu.xeppaka.bot

sealed trait PostType

object PostTypes {
  case object CzechPost extends PostType
  case object PplPost extends PostType
}
