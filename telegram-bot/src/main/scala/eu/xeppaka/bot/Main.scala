package eu.xeppaka.bot

import akka.actor.Scheduler
import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.{ActorSystem, DispatcherSelector, SupervisorStrategy}
import akka.http.scaladsl.Http
import akka.util.Timeout
import akka.{Done, actor}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.io.StdIn

object Main {
  def main(args: Array[String]): Unit = {
    //val botId = "570855144:AAEv7b817cuq2JJI9f2kG5B9G3zW1x-btz4"  // useless bot
    val botId = "693134480:AAE8JRXA6j1mkOKTaxapP6A-E4LPHRuiIf8"    // delivery bot
    val botBehavior = Behaviors.supervise(TelegramBot.behavior(botId, "0.0.0.0", 88)).onFailure(SupervisorStrategy.restart)
    val telegramBot = ActorSystem(botBehavior, "telegram-bot")
    implicit val actorSystem: actor.ActorSystem = telegramBot.toUntyped
    implicit val executionContext: ExecutionContextExecutor = telegramBot.dispatchers.lookup(DispatcherSelector.default())
    implicit val scheduler: Scheduler = telegramBot.scheduler
    implicit val timeout: Timeout = 10.seconds

    println("Press enter to finish bot...")
    StdIn.readLine()

    val stopFuture: Future[Done] = telegramBot ? (ref => TelegramBot.Stop(ref))

    val terminateFuture = stopFuture
      .andThen { case _ => Http().shutdownAllConnectionPools() }
      .andThen { case _ => telegramBot.terminate() }

    Await.ready(terminateFuture, 20.seconds)
  }
}
