package eu.xeppaka.bot

import akka.http.scaladsl.model.Uri

case class BotUri(botId: String) {
  private val baseUri = Uri(s"https://api.telegram.org/bot$botId")

  val botUri: Uri = baseUri
  val getMe: Uri = baseUri.withPath(baseUri.path / "getMe")
  val setWebhook: Uri = baseUri.withPath(baseUri.path / "setWebhook")
  val deleteWebhook: Uri = baseUri.withPath(baseUri.path / "deleteWebhook")
  val getWebhookInfo: Uri = baseUri.withPath(baseUri.path / "getWebhookInfo")
  val sendMessage: Uri = baseUri.withPath(baseUri.path / "sendMessage")
  val editMessageReplyMarkup: Uri = baseUri.withPath(baseUri.path / "editMessageReplyMarkup")
}
