package eu.xeppaka.bot

import java.security.cert.X509Certificate
import java.text.SimpleDateFormat

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.{Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior, DispatcherSelector}
import akka.http.scaladsl.UseHttp2.Negotiated
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Accept, `User-Agent`}
import akka.http.scaladsl.settings.{ClientConnectionSettings, ConnectionPoolSettings}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.{Http, HttpsConnectionContext}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.EventSourcedBehavior.{CommandHandler, EventHandler}
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import akka.stream.ActorMaterializer
import com.typesafe.sslconfig.akka.AkkaSSLConfig
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import javax.net.ssl.{KeyManager, SSLContext, X509TrustManager}

import scala.collection.immutable
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Entities {

  case class Attributes(
    parcelType: String,
    weight: Double,
    currency: String,
  )

  case class State(
    id: String,
    date: String,
    text: String,
    postcode: Option[String],
    postoffice: Option[String],
    idIcon: Option[Int],
    publicAccess: Int,
    latitude: Option[Double],
    longitude: Option[Double],
    timeDeliveryAttempt: Option[String]
  )

  case class States(state: Seq[State])

  case class ParcelHistory(id: String, attributes: Attributes, states: States)
}

object CzechPostDeliveryCheck {
  private val czechPostDateFormat = new SimpleDateFormat("yyyy-MM-dd")
  private val printDateFormat = new SimpleDateFormat("dd-MM-yyyy")

  sealed trait Command
  sealed trait CommandResult
  sealed trait Event
  case class ParcelState(attributes: Option[Entities.Attributes] = None, states: Set[Entities.State] = Set.empty) {
    def prettyPrint(parcelId: String): String = {
      val statesString = states
        .toSeq
        .sortBy(state => czechPostDateFormat.parse(state.date))
        .map(state => s"${printDateFormat.format(czechPostDateFormat.parse(state.date))} - ${state.text}\n===========================\n")
        .mkString

      s"""|*New state(s) of the parcel $parcelId:*
          |===========================
          |$statesString""".stripMargin
    }
  }
  case class State(parcelStates: Map[String, ParcelState] = Map.empty)

  case class AddParcel(parcelId: String, replyTo: ActorRef[CommandResult]) extends Command
  case class RemoveParcel(parcelId: String, replyTo: ActorRef[CommandResult]) extends Command
  case class ListParcels(replyTo: ActorRef[ListParcelsResult]) extends Command
  case class ListParcelsResult(parcelsList: Set[String])

  case object CommandResultSuccess extends CommandResult
  case class CommandResultFailure(exception: Throwable) extends CommandResult

  case class ParcelIdNotFound(parcelId: String) extends Exception
  case class DuplicateParcelId(parcelId: String) extends Exception

  // internal commands
  private case object CheckParcels extends Command
  private case class ParcelHistoryRetrieved(parcelHistory: Entities.ParcelHistory) extends Command
  case class DeliveryStateChanged(state: String)

  case class ParcelAdded(parcelId: String) extends Event
  case class ParcelRemoved(parcelId: String) extends Event
  case class ParcelHistoryStateAdded(parcelId: String, state: Entities.State) extends Event
  case class ParcelAttributesChanged(parcelId: String, attributes: Entities.Attributes) extends Event

  private val trustfulSslContext: SSLContext = {
    object NoCheckX509TrustManager extends X509TrustManager {
      override def checkClientTrusted(chain: Array[X509Certificate], authType: String): Unit = ()
      override def checkServerTrusted(chain: Array[X509Certificate], authType: String): Unit = ()
      override def getAcceptedIssuers: Array[X509Certificate] = Array[X509Certificate]()
    }

    val context = SSLContext.getInstance("TLS")
    context.init(Array[KeyManager](), Array(NoCheckX509TrustManager), null)
    context
  }

  def behavior(chatId: String, stateReporter: ActorRef[DeliveryStateChanged]): Behavior[Command] = Behaviors.setup[Command] { ctx =>
    Behaviors.withTimers(scheduler => checkParcel(chatId, stateReporter, scheduler))
  }

  private def checkParcel(chatId: String, stateReporter: ActorRef[DeliveryStateChanged], scheduler: TimerScheduler[Command]): Behavior[Command] = Behaviors.setup { ctx =>
    implicit val actorSystem: ActorSystem = ctx.system.toUntyped
    implicit val executionContext: ExecutionContextExecutor = ctx.system.dispatchers.lookup(DispatcherSelector.default())
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    val http = Http()
    val badSslConfig = AkkaSSLConfig().mapSettings(s => s.withLoose(s.loose
      .withAcceptAnyCertificate(true)
      .withDisableHostnameVerification(true)))
    val originalCtx = http.createClientHttpsContext(badSslConfig)
    val sslContext = new HttpsConnectionContext(
      trustfulSslContext,
      originalCtx.sslConfig,
      originalCtx.enabledCipherSuites,
      originalCtx.enabledProtocols,
      originalCtx.clientAuth,
      originalCtx.sslParameters,
      Negotiated
    )
    val clientConnectionSettings = ClientConnectionSettings(actorSystem).withUserAgentHeader(Some(`User-Agent`("Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0")))
    val connectionSettings = ConnectionPoolSettings(actorSystem).withConnectionSettings(clientConnectionSettings)

    scheduler.startPeriodicTimer("check-delivery-state", CheckParcels, 5.minutes)

    val commandHandler: CommandHandler[Command, Event, State] = (state, cmd) => {
      cmd match {
        case AddParcel(parcelId, replyTo) =>
          val parcelIdUpper = parcelId.toUpperCase
          if (state.parcelStates.keySet.contains(parcelIdUpper)) {
            Effect
              .none
              .thenRun(_ => replyTo ! CommandResultFailure(DuplicateParcelId(parcelIdUpper)))
          } else {
            Effect
              .persist(ParcelAdded(parcelIdUpper))
              .thenRun(_ => {
                replyTo ! CommandResultSuccess
                ctx.self ! CheckParcels
              })
          }
        case RemoveParcel(parcelId, replyTo) =>
          val parcelIdUpper = parcelId.toUpperCase
          if (state.parcelStates.contains(parcelIdUpper)) {
            Effect
              .persist(ParcelRemoved(parcelIdUpper))
              .thenRun(_ => replyTo ! CommandResultSuccess)
          } else {
            Effect
              .none
              .thenRun(_ => replyTo ! CommandResultFailure(ParcelIdNotFound(parcelIdUpper)))
          }

        case ListParcels(replyTo) =>
          Effect.none
            .thenRun { state =>
              val parcelsList = state.parcelStates.keySet
              replyTo ! ListParcelsResult(parcelsList)
            }

        case CheckParcels =>
          Effect
            .none
            .thenRun { _ =>
              ctx.log.info("action=check_parcel_state chat_id={}", chatId)
              val parcelIds = state.parcelStates.keys.grouped(10).map(ids => ids.foldLeft("")((acc, id) => if (acc.isEmpty) id else s"$acc;$id"))

              for (ids <- parcelIds) {
                val checkUri = Uri(s"https://b2c.cpost.cz/services/ParcelHistory/getDataAsJson?idParcel=$ids&language=cz")
                val request = HttpRequest(uri = checkUri, headers = immutable.Seq(Accept(MediaTypes.`application/json`)))

                ctx.log.info("action=check_parcel_state chat_id={} check_uri={}", chatId, checkUri)

                http
                  .singleRequest(request, connectionContext = sslContext, settings = connectionSettings)
                  .transform {
                    case Success(response) => if (response.status.isSuccess()) Success(response) else Failure(new Exception(s"Check parcel returned HTTP status: ${response.status.value}."))
                    case response: Failure[HttpResponse] => response
                  }
                  .flatMap(response => Unmarshal(response).to[Array[Entities.ParcelHistory]])
                  .andThen {
                    case Success(parcelHistories) =>
                      parcelHistories.foreach(parcelHistory => ctx.self ! ParcelHistoryRetrieved(parcelHistory))
                    case Failure(exception) =>
                      ctx.log.error(exception, "Error checking parcel history.")
                  }
                  .andThen {
                    case Success(_) => ctx.log.info("action=check_parcel_state result=success chat_id={} check_uri={}", chatId, checkUri)
                    case Failure(exception) => ctx.log.error(exception, "action=check_parcel_state result=failure chat_id={} check_uri={}", chatId, checkUri)
                  }
              }
            }
        case ParcelHistoryRetrieved(parcelHistory) =>
          val parcelId = parcelHistory.id
          val parcelState = state.parcelStates(parcelId)
          val attributesChangedEvent = (if (parcelState.attributes.isEmpty)
                                          Some(parcelHistory.attributes)
                                        else
                                          parcelState.attributes
                                            .flatMap(oldAttributes => if (oldAttributes != parcelHistory.attributes) Some(parcelHistory.attributes) else None))
            .map(attributes => ParcelAttributesChanged(parcelId, attributes)).to[collection.immutable.Seq]

          val newStates = parcelHistory.states.state.toSet -- parcelState.states
          val stateEvents: Seq[Event] = newStates.map(state => ParcelHistoryStateAdded(parcelId, state)).to[collection.immutable.Seq]

          Effect
            .persist(attributesChangedEvent ++ stateEvents)
            .thenRun(_ => {
              if (newStates.nonEmpty) {
                stateReporter ! DeliveryStateChanged(ParcelState(None, newStates).prettyPrint(parcelId))
              }
            })
      }
    }

    val eventHandler: EventHandler[State, Event] = (state, evt) => {
      evt match {
        case ParcelAdded(parcelId) => state.copy(parcelStates = state.parcelStates + (parcelId -> ParcelState()))
        case ParcelRemoved(parcelId) => state.copy(parcelStates = state.parcelStates - parcelId)
        case ParcelHistoryStateAdded(parcelId, newState) =>
          val parcelState = state.parcelStates(parcelId)
          val newParcelState = parcelState.copy(states = parcelState.states + newState)
          state.copy(parcelStates = state.parcelStates.updated(parcelId, newParcelState))
        case ParcelAttributesChanged(parcelId, newAttributes) =>
          val parcelState = state.parcelStates(parcelId)
          val newParcelState = parcelState.copy(attributes = Some(newAttributes))
          state.copy(parcelStates = state.parcelStates.updated(parcelId, newParcelState))
      }
    }

    EventSourcedBehavior[Command, Event, State](
      persistenceId = PersistenceId(s"$chatId-czechpost"),
      emptyState = State(),
      commandHandler = commandHandler,
      eventHandler = eventHandler
    )
  }
}
