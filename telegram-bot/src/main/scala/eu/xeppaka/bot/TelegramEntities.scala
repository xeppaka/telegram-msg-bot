package eu.xeppaka.bot

object TelegramEntities {

  case class Response[T](ok: Boolean,
    description: Option[String] = None,
    error_code: Option[Int] = None,
    result: T
  )

  case class GetMe(id: Int, is_bot: Boolean, first_name: String, username: String)

  case class KeyboardButton(text: String,
    request_contact: Option[Boolean] = None,
    request_location: Option[Boolean] = None
  )

  case class InlineKeyboardButton(text: String,
    url: Option[String] = None,
    callback_data: Option[String] = None,
    switch_inline_query: Option[String] = None,
    switch_inline_query_current_chat: Option[String] = None,
    callback_game: Option[String] = None,
    pay: Option[Boolean] = None
  )

  sealed trait ReplyMarkup

  case class ReplyKeyboardRemove(remove_keyboard: Boolean = true, selective: Option[Boolean] = None) extends ReplyMarkup

  case class ReplyKeyboardMarkup(keyboard: Seq[Seq[KeyboardButton]],
    resize_keyboard: Option[Boolean] = None,
    one_time_keyboard: Option[Boolean] = None,
    selective: Option[Boolean] = None
  ) extends ReplyMarkup

  case class InlineKeyboardMarkup(inline_keyboard: Seq[Seq[InlineKeyboardButton]])
    extends ReplyMarkup

  case class ForceReply(force_reply: Boolean = true, selective: Option[Boolean] = None) extends ReplyMarkup

  case class InlineQuery(id: String,
    from: User,
    location: Location,
    query: String,
    offset: String
  )

  case class Location(longitude: Float,
    latitude: Float
  )

  case class Update(update_id: Int,
    message: Option[Message] = None,
    edited_message: Option[Message] = None,
    channel_post: Option[Message] = None,
    edited_channel_post: Option[Message] = None,
    inline_query: Option[InlineQuery] = None,
    chosen_inline_result: Option[ChosenInlineResult] = None,
    callback_query: Option[CallbackQuery] = None,
    shipping_query: Option[ShippingQuery] = None,
    pre_checkout_query: Option[PreCheckoutQuery] = None
  )

  case class ChosenInlineResult(result_id: String,
    from: User,
    location: Option[Location] = None,
    inline_message_id: Option[String] = None,
    query: String
  )

  case class CallbackQuery(id: String,
    from: User,
    message: Option[Message] = None,
    inline_message_id: Option[String] = None,
    chat_instance: String,
    data: Option[String] = None,
    game_short_name: Option[String] = None
  )

  case class ShippingQuery(id: String,
    from: User,
    invoice_payload: String,
    shipping_address: ShippingAddress
  )

  case class ShippingAddress(country_code: String,
    state: String,
    city: String,
    street_line1: String,
    street_line2: String,
    post_code: String
  )

  case class PreCheckoutQuery(id: String,
    from: User,
    currency: String,
    total_amount: Int,
    invoice_payload: String,
    shipping_option_id: Option[String] = None,
    order_info: Option[OrderInfo] = None
  )

  case class OrderInfo(name: Option[String] = None,
    phone_number: Option[String] = None,
    email: Option[String] = None,
    shipping_address: Option[ShippingAddress] = None
  )

  case class User(id: Int,
    is_bot: Boolean,
    first_name: String,
    last_name: Option[String] = None,
    username: Option[String] = None,
    language_code: Option[String] = None
  )

  case class EditMessageReplyMarkup(chat_id: Option[Long],
    message_id: Option[Int],
    inline_message_id: Option[String],
    reply_markup: Option[InlineKeyboardMarkup]
  )

  case class SendMessage(chat_id: Long,
    text: String,
    parse_mode: Option[String] = None,
    disable_web_page_preview: Option[Boolean] = None,
    disable_notification: Option[Boolean] = None,
    reply_to_message_id: Option[Int] = None,
    reply_markup: Option[ReplyMarkup] = None
  )

  case class Message(message_id: Int,
    from: Option[User] = None,
    date: Int,
    chat: Chat,
    forward_from: Option[User] = None,
    forward_from_chat: Option[User] = None,
    forward_from_message_id: Option[Int] = None,
    forward_signature: Option[String] = None,
    forward_date: Option[Int] = None,
    reply_to_message: Option[Message] = None,
    edit_date: Option[Int] = None,
    media_group_id: Option[String] = None,
    author_signature: Option[String] = None,
    text: Option[String] = None,
    entities: Option[Seq[MessageEntity]] = None,
    caption_entities: Option[Seq[MessageEntity]] = None,
    audio: Option[Audio] = None,
    document: Option[Document] = None,
    game: Option[Game] = None,
    photo: Option[Seq[PhotoSize]] = None,
    sticker: Option[Sticker] = None,
    video: Option[Video] = None,
    voice: Option[Voice] = None,
    video_note: Option[VideoNote] = None,
    caption: Option[String] = None,
    contact: Option[Contact] = None,
    location: Option[Location] = None,
    venue: Option[Venue] = None,
    new_chat_members: Option[Seq[User]] = None,
    left_chat_member: Option[Seq[User]] = None,
    new_chat_title: Option[String] = None,
    new_chat_photo: Option[Seq[PhotoSize]] = None,
    delete_chat_photo: Option[Boolean] = None,
    group_chat_created: Option[Boolean] = None,
    supergroup_chat_created: Option[Boolean] = None,
    channel_chat_created: Option[Boolean] = None,
    migrate_to_chat_id: Option[Int] = None,
    migrate_from_chat_id: Option[Int] = None,
    pinned_message: Option[Message] = None,
    invoice: Option[Invoice] = None,
    successful_payment: Option[SuccessfulPayment] = None,
    connected_website: Option[String] = None
  )

  case class MessageEntity(`type`: String,
    offset: Int,
    length: Int,
    url: Option[String] = None,
    user: Option[User] = None
  )

  case class Contact(phone_number: String,
    first_name: String,
    last_name: Option[String] = None,
    user_id: Option[Int] = None
  )

  case class Sticker(file_id: String,
    width: Int,
    height: Int,
    thumb: Option[PhotoSize] = None,
    emoji: Option[String] = None,
    set_name: Option[String] = None,
    mask_position: Option[String] = None,
    file_size: Option[Int] = None
  )

  case class Video(file_id: String,
    width: Int,
    height: Int,
    duration: Int,
    thumb: Option[PhotoSize] = None,
    mime_type: Option[String] = None,
    file_size: Option[Int] = None
  )

  case class Audio(file_id: String,
    duration: Int,
    performer: Option[String] = None,
    title: Option[String] = None,
    mime_type: Option[String] = None,
    file_size: Option[Int] = None
  )

  case class Document(file_id: String,
    thumb: Option[PhotoSize] = None,
    file_name: Option[String] = None,
    mime_type: Option[String] = None,
    file_size: Option[Int] = None
  )

  case class PhotoSize(file_id: String,
    width: Int,
    height: Int,
    file_size: Option[Int] = None
  )

  case class Voice(file_id: String,
    duration: Int,
    mime_type: Option[String] = None,
    file_size: Option[Int] = None
  )

  case class VideoNote(file_id: String,
    length: Int,
    duration: Int,
    thumb: Option[PhotoSize] = None,
    file_size: Option[Int] = None
  )

  case class ChatPhoto(small_file_id: String, big_file_id: String)

  case class Chat(id: Long,
    `type`: String,
    title: Option[String] = None,
    username: Option[String] = None,
    first_name: Option[String] = None,
    last_name: Option[String] = None,
    all_members_are_administrators: Option[Boolean] = None,
    photo: Option[ChatPhoto] = None,
    description: Option[String] = None,
    invite_link: Option[String] = None,
    pinned_message: Option[Message] = None,
    sticker_set_name: Option[String] = None,
    can_set_sticker_set: Option[Boolean] = None
  )

  case class Game(title: String,
    description: String,
    photo: Seq[PhotoSize],
    text: Option[String] = None,
    text_entities: Option[Seq[MessageEntity]] = None,
    animation: Option[Animation] = None
  )

  case class Animation(file_id: String,
    thumb: Option[PhotoSize] = None,
    file_name: Option[String] = None,
    mime_type: Option[String] = None,
    file_size: Option[Int] = None
  )

  case class InputFile()

  case class Venue(location: Location,
    title: String,
    address: String,
    foursquare_id: Option[String] = None
  )

  case class Invoice(title: String,
    description: String,
    start_parameter: String,
    currency: String,
    total_amount: Int
  )

  case class SuccessfulPayment(currency: String,
    total_amount: Int,
    invoice_payload: String,
    shipping_option_id: Option[String] = None,
    order_info: Option[OrderInfo] = None,
    telegram_payment_charge_id: String,
    provider_payment_charge_id: String
  )

  case class Webhook(url: String,
    certificate: Option[InputFile] = None,
    max_connections: Option[Int] = None,
    allowed_updates: Option[Seq[String]] = None
  )

  case class WebhookInfo(url: String,
    has_custom_certificate: Boolean,
    pending_update_count: Int,
    last_error_date: Option[Int] = None,
    last_error_message: Option[String] = None,
    max_connections: Option[Int] = None,
    allowed_updates: Option[Seq[String]] = None
  )
}
