package eu.xeppaka.bot

import cats.syntax.functor._
import eu.xeppaka.bot.TelegramEntities._
import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._
import io.circe.syntax._

object TelegramEntitiesDerivations {
  implicit val encodeReplyMarkup: Encoder[ReplyMarkup] = Encoder.instance {
    case replyKeyboardMarkup: ReplyKeyboardMarkup => replyKeyboardMarkup.asJson
    case replyKeyboardRemove: ReplyKeyboardRemove => replyKeyboardRemove.asJson
    case inlineKeyboardMarkup: InlineKeyboardMarkup => inlineKeyboardMarkup.asJson
    case forceReply: ForceReply => forceReply.asJson
  }

  implicit val decodeReplyMarkup: Decoder[ReplyMarkup] =
    List[Decoder[ReplyMarkup]](
      Decoder[ReplyKeyboardMarkup].widen,
      Decoder[ReplyKeyboardRemove].widen,
      Decoder[InlineKeyboardMarkup].widen,
      Decoder[ForceReply].widen
    ).reduceLeft(_ or _)
}
