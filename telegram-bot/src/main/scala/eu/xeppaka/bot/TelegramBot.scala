package eu.xeppaka.bot

import java.io.InputStream
import java.security.{KeyStore, SecureRandom}
import java.util.UUID

import akka.Done
import akka.actor.{ActorSystem, Scheduler}
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.{Behaviors, StashBuffer}
import akka.actor.typed.{ActorRef, Behavior, DispatcherSelector, SupervisorStrategy}
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{as, complete, entity, extractLog, onComplete, path, post}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.{ConnectionContext, Http, HttpExt, HttpsConnectionContext}
import akka.stream.ActorMaterializer
import akka.util.{ByteString, Timeout}
import eu.xeppaka.bot.TelegramEntities._
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}

import scala.collection.immutable
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.io.Source
import scala.util.{Failure, Success}

object TelegramBot {
  sealed trait Command
  sealed trait CommandResult
  sealed trait StopResult extends CommandResult

  case class Stop(replyTo: ActorRef[Done]) extends Command
  case object GetBotInfo
  case object GetWebhookInfo

  def behavior(botId: String, interface: String, port: Int): Behavior[Command] = Behaviors.setup[Command] { ctx =>
    ctx.log.info("action=start_bot")

    implicit val untypedSystem: ActorSystem = ctx.system.toUntyped
    implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()
    implicit val executionContextExecutor: ExecutionContextExecutor = ctx.system.dispatchers.lookup(DispatcherSelector.default())

    val botUri = BotUri(botId)
    val http: HttpExt = Http()
    val hookId = UUID.randomUUID().toString
    val webhookUri = Uri(s"https://xeppaka.eu:88/$hookId")
    val httpsContext = createHttpsConnectionContext
    val stashBuffer = StashBuffer[Command](10)
    val dialogManager = ctx.spawnAnonymous(Behaviors.supervise(DialogManager.behavior(botUri)).onFailure(SupervisorStrategy.restart))
    val routes = botRoutes(hookId, dialogManager)(untypedSystem.scheduler)

    def bindingServer: Behavior[Command] = Behaviors.setup[Command] { ctx =>
      case class BindingSuccess(binding: Http.ServerBinding) extends Command
      case class BindingFailure(exception: Throwable) extends Command

      ctx.log.info("action=bind_server interface={} port={}", interface, port)

      http
        .bindAndHandle(routes, interface, port, httpsContext)
        .onComplete {
          case Success(binding) => ctx.self ! BindingSuccess(binding)
          case Failure(exception) => ctx.self ! BindingFailure(exception)
        }

      Behaviors.receiveMessage[Command] {
        case BindingSuccess(binding) =>
          ctx.log.info("action=bind_server result=success")
          settingWebhook(binding)
        case BindingFailure(exception) =>
          ctx.log.error("action=bind_server result=failure", exception)
          ctx.log.error("action=start_bot result=failure")
          Behaviors.stopped
        case otherCommand: Command =>
          stashBuffer.stash(otherCommand)
          Behaviors.same
      }
    }

    def unbindingServer(binding: Http.ServerBinding, replyTo: Option[ActorRef[Done]]): Behavior[Command] = Behaviors.setup[Command] { ctx =>
      case object UnbindingSuccess extends Command
      case class UnbindingFailure(exception: Throwable) extends Command

      ctx.log.info("action=unbind_server interface={} port={}", interface, port)

      binding
        .unbind()
        .onComplete {
          case Success(Done) => ctx.self ! UnbindingSuccess
          case Failure(exception) => ctx.self ! UnbindingFailure(exception)
        }

      Behaviors.receiveMessage[Command] {
        case UnbindingSuccess =>
          ctx.log.info("action=unbind_server result=success")
          replyTo.foreach(_ ! Done)
          Behaviors.stopped
        case UnbindingFailure(exception) =>
          ctx.log.error("action=unbind_server result=failure", exception)
          replyTo.foreach(_ ! Done)
          Behaviors.stopped
        case _ => Behaviors.unhandled
      }
    }

    def settingWebhook(binding: Http.ServerBinding): Behavior[Command] = Behaviors.setup[Command] { ctx =>
      case object SetWebhookSuccess extends Command
      case class SetWebhookFailure(exception: Throwable) extends Command

      ctx.log.info("action=set_webhook url={} webhook={}", botUri.setWebhook, webhookUri)

      implicit val executionContextExecutor: ExecutionContextExecutor = ctx.system.dispatchers.lookup(DispatcherSelector.default())

      val urlEntity = HttpEntity.Strict(ContentTypes.`text/plain(UTF-8)`, ByteString(webhookUri.toString()))
      val urlPart = Multipart.FormData.BodyPart.Strict("url", urlEntity)

      val certificate = ByteString(Source.fromResource("telegram-bot.pem").mkString)
      val certificateEntity = HttpEntity.Strict(ContentTypes.`application/octet-stream`, certificate)
      val certificatePart = Multipart.FormData.BodyPart.Strict("certificate", certificateEntity, Map("filename" -> "telegram-bot.pem"))

      val setWebhookFormData = Multipart.FormData.Strict(immutable.Seq(urlPart, certificatePart))

      Marshal(setWebhookFormData)
        .to[RequestEntity]
        .flatMap(requestEntity => http.singleRequest(HttpRequest(uri = botUri.setWebhook, method = HttpMethods.POST, entity = requestEntity)))
        .onComplete {
          case Success(response) =>
            if (response.status.isSuccess())
              ctx.self ! SetWebhookSuccess
            else
              ctx.self ! SetWebhookFailure(new RuntimeException(s"Set webhook HTTP response status: ${response.status.value}."))
          case Failure(exception) =>
            ctx.self ! SetWebhookFailure(exception)
        }

      Behaviors.receiveMessage {
        case SetWebhookSuccess =>
          ctx.log.info("action=set_webhook result=success")
          stashBuffer.unstashAll(ctx, started(binding))
        case SetWebhookFailure(exception) =>
          ctx.log.error("action=set_webhook result=failure", exception)
          ctx.log.error("action=start_bot result=failure")
          unbindingServer(binding, None)
        case otherCommand: Command =>
          stashBuffer.stash(otherCommand)
          Behaviors.same
      }
    }

    def deletingWebhook(binding: Http.ServerBinding, replyTo: ActorRef[Done]): Behavior[Command] = Behaviors.setup[Command] { ctx =>
      case object DeleteWebhookSuccess extends Command
      case class DeleteWebhookFailure(exception: Throwable) extends Command

      ctx.log.info("action=delete_webhook url={} webhook={}", botUri.deleteWebhook, webhookUri)

      implicit val executionContextExecutor: ExecutionContextExecutor = ctx.system.dispatchers.lookup(DispatcherSelector.default())

      http
        .singleRequest(HttpRequest(uri = botUri.deleteWebhook, method = HttpMethods.POST))
        .onComplete {
          case Success(response) =>
            if (response.status.isSuccess())
              ctx.self ! DeleteWebhookSuccess
            else
              ctx.self ! DeleteWebhookFailure(new RuntimeException(s"Delete webhook HTTP response status: ${response.status.value}"))
          case Failure(exception) =>
            ctx.self ! DeleteWebhookFailure(exception)
        }

      Behaviors.receiveMessage {
        case DeleteWebhookSuccess =>
          ctx.log.info("action=delete_webhook result=success")
          unbindingServer(binding, Some(replyTo))
        case DeleteWebhookFailure(exception) =>
          ctx.log.error("action=delete_webhook result=failure", exception)
          unbindingServer(binding, Some(replyTo))
        case _ => Behaviors.unhandled
      }
    }

    def started(binding: Http.ServerBinding): Behavior[Command] = Behaviors.setup[Command] { ctx =>
      ctx.log.info("action=start_bot result=success")

      Behaviors.receiveMessage[Command] {
        case stopCommand@Stop(replyTo) =>
          ctx.log.info("action=stop_bot")
          deletingWebhook(binding, replyTo)
        case _ =>
          Behaviors.unhandled
      }
    }

    bindingServer
  }

  private def botRoutes(hookId: String, updatesProcessor: ActorRef[DialogManager.ProcessUpdate])(implicit scheduler: Scheduler): Route = {
    import akka.actor.typed.scaladsl.AskPattern._
    import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
    import io.circe.generic.auto._

    implicit val timeout: Timeout = 30.seconds

    path(hookId) {
      post {
        extractLog { log =>
          entity(as[Update]) { update =>
            onComplete(updatesProcessor.?[DialogManager.CommandResult](ref => DialogManager.ProcessUpdate(update, ref))) {
              case Success(processResult) => processResult match {
                case DialogManager.ProcessUpdateSuccess => complete(HttpResponse(status = StatusCodes.OK))
                case DialogManager.ProcessUpdateFailure(exception) =>
                  log.error(exception, "action=process_update result=failure message={}", update)
                  complete(HttpResponse(status = StatusCodes.InternalServerError))
              }
              case Failure(exception) =>
                log.error(exception, "action=process_update result=failure message={}", update)
                complete(HttpResponse(status = StatusCodes.InternalServerError))
            }
          }
        }
      }
    }
  }

  private def createHttpsConnectionContext: HttpsConnectionContext = {
    val password: Array[Char] = "".toCharArray // do not store passwords in code, read them from somewhere safe!

    val ks: KeyStore = KeyStore.getInstance("PKCS12")
    val keystore: InputStream = getClass.getResourceAsStream("/telegram-bot.p12")

    require(keystore != null, "Keystore required!")
    ks.load(keystore, password)

    val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, password)

    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    tmf.init(ks)

    val sslContext: SSLContext = SSLContext.getInstance("TLS")
    sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, new SecureRandom)

    ConnectionContext.https(sslContext)
  }
}
