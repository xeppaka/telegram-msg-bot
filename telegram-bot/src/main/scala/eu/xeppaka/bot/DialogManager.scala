package eu.xeppaka.bot

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, SupervisorStrategy}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.EventSourcedBehavior.{CommandHandler, EventHandler}
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import akka.util.Timeout
import eu.xeppaka.bot.CheckDeliveryDialog.{ProcessMessageFailure, ProcessMessageSuccess}
import eu.xeppaka.bot.TelegramEntities.Update

import scala.concurrent.duration._
import scala.util.{Failure, Success}

object DialogManager {
  sealed trait Command
  sealed trait CommandResult

  case class ProcessUpdate(update: Update, replyTo: ActorRef[CommandResult]) extends Command
  case object ProcessUpdateSuccess extends CommandResult
  case class ProcessUpdateFailure(exception: Throwable) extends CommandResult

  // internal messages
  private case class DialogResponseSuccess(dialogId: Long, replyTo: ActorRef[CommandResult]) extends Command
  private case class DialogResponseFailure(dialogId: Long, exception: Throwable, replyTo: ActorRef[CommandResult]) extends Command

  sealed trait Event
  private case class DialogAdded(chatId: Long) extends Event

  case class State(dialogs: Map[Long, ActorRef[CheckDeliveryDialog.Command]] = Map.empty)

  def behavior(botUri: BotUri): Behavior[Command] = Behaviors.setup[Command] { ctx =>
    val commandHandler: CommandHandler[Command, Event, State] = (state, cmd) => {
      cmd match {
        case ProcessUpdate(update, replyTo) =>
          if (update.message.isDefined) {
            val chatId = update.message.get.chat.id

            val effect: Effect[Event, State] = if (state.dialogs.contains(chatId)) {
              Effect.none
            } else {
              Effect.persist(DialogAdded(chatId))
            }

            effect
              .thenRun(_ => ctx.log.debug("action=process_update chat_id={} message={}", chatId, update.message.get))
              .thenRun { state =>
                val msg = update.message.get
                val dialogActor = state.dialogs(chatId)

                ctx.log.info("action=ask_dialog id={}", chatId)

                implicit val timeout: Timeout = 20.seconds
                ctx.ask(dialogActor)((CheckDeliveryDialog.ProcessMessage.apply _).curried(msg)) {
                  case Success(ProcessMessageSuccess) => DialogResponseSuccess(chatId, replyTo)
                  case Success(ProcessMessageFailure(exception)) => DialogResponseFailure(chatId, exception, replyTo)
                  case Failure(exception) => DialogResponseFailure(chatId, exception, replyTo)
                }
            }
          } else {
            Effect
              .none
              .thenRun { _ =>
                ctx.log.debug("action=process_update result=success message=update message is empty")
              }
          }

        case DialogResponseSuccess(dialogId, replyTo) =>
          Effect
            .none
            .thenRun { _ =>
              ctx.log.info("action=ask_dialog id={} result=success", dialogId)
              replyTo ! ProcessUpdateSuccess
            }
        case DialogResponseFailure(dialogId, exception, replyTo) =>
          Effect
              .none
              .thenRun { _ =>
                ctx.log.error(exception, "action=ask_dialog id={} result=failure", dialogId)
                replyTo ! ProcessUpdateFailure(exception)
              }
      }
    }

    val eventHandler: EventHandler[State, Event] = (state, evt) => {
      evt match {
        case DialogAdded(chatId) =>
          val dialogActor = ctx.spawn(Behaviors.supervise(CheckDeliveryDialog.behavior(chatId, botUri)).onFailure(SupervisorStrategy.restart), s"delivery-check-$chatId")
          state.copy(dialogs = state.dialogs.updated(chatId, dialogActor))
      }
    }

    EventSourcedBehavior(
      persistenceId = PersistenceId("dialog-manager"),
      emptyState = State(),
      commandHandler = commandHandler,
      eventHandler = eventHandler
    )
  }
}
