import Dependencies._

lazy val commonSettings = Seq(
  organization := "com.example",
  scalaVersion := "2.12.8",
  version := "0.1.0-SNAPSHOT",
  mainClass := Some("eu.xeppaka.bot.Main")
)

inThisBuild(commonSettings)

lazy val `telegram-bot` = (project in file("telegram-bot"))
  .settings(
    libraryDependencies ++= Seq(
      scalaTest % Test,
      akka,
      akkaTyped,
      akkaHttp,
      akkaStream,
      akkaPersistence,
      levelDbJni,
      circleCore,
      circleGeneric,
      circleParser,
      circeAkkaHttp
    )
  )
